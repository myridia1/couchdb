Share your Geo Location as Latitude and Longitude with your own template.
* Large accessible buttons to share quick your latitude and longitude
* Create your own share template with an editor
* Use net or satellite native app services what works on offline mode
* Quick to use when user is on the field
