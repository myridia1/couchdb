## Couchdb Editor 
<img src="https://w3.calantas.org/wp-content/uploads/2022/06/RoseSimple.png" width="90" height="90">

Share your Geo Location as Latitude and Longitude with your own template.

You can customize a template with the latitude and longitude and share it to other apps.

### Features
* Large accessible buttons to share quick your latitude and longitude
* Create your own share template with an editor
* Use net or satellite native app services what works on offline mode
* Quick to use when user is on the field 

### Download the Source code 
```console
git config --global credential.helper store
git clone https://gitlab.com/myridia1/couchdb.git
cd couchdb
```

### How do I set it up? 

#### Download the sdk 
https://developer.android.com/studio/#command-tools look for a commandline tool like: sdk-tools-linux-4333796.zip

#### Unzip the downloaded file: 
```bash
unzip commandlinetools-linux-xxxxx_latest.zip
``` 

#### Create the folders android-sdk/platform-tools in /opt/
```bash
sudo mkdir -p /opt/android-sdk/cmdline-tools/
```

#### Move the folder cmdline-tools to the created folder and name it tools/ 
```bash
sudo cp cmdline-tools /opt/android-sdk/cmdline-tools/tools
```

#### Make it accessble for all users: 
```bash
sudo chmod 777 /opt/android-sdk -Rf
```

#### Your folder structure should look like like: 
```text
├── android-sdk
│   ├── cmdline-tools
│   │   └── tools
│   │       ├── bin
│   │       ├── lib
│   │       ├── NOTICE.txt
│   │       └── source.properties

```

#### Check where your java-11-openjdk is installed and eventual create syslink if the name is different
```bash
ln -s /usr/lib/jvm/java-17-openjdk-amd64 /usr/lib/jvm/java-17-openjdk
```


#### Edit your ~/.bashrc file like below to have the enviorment variable loaded, 
```bash
export JAVA_HOME='/usr/lib/jvm/java-17-openjdk'  
export ANDROID_HOME="/opt/android-sdk"  
export PATH=$PATH:$ANDROID_HOME/cmdline-tools  
export PATH=$PATH:$ANDROID_HOME/cmdline-tools/tools/bin  
export PATH=$PATH:$ANDROID_HOME/platform-tools/bin  
```

#### Reload your enviroment with: 
```bash
source ~/.bashrc 
```
### Find out the lastest build-tools 
 * https://developer.android.com/tools/releases/build-tools
 
### Find out the lastest platform
 * https://developer.android.com/tools/releases/platforms
 
#### Execute the below sdkmanager commands 
```bash
yes | sdkmanager --licenses
sdkmanager ndk-bundle
sdkmanager "build-tools;34.0.0"
sdkmanager "platform-tools" "platforms;android-34"
```

### Compile the debug APK 
```bash
./gradlew assembleDebug
```

### Compile an unsigned release APK
```bash
./gradlew assembleRelease
```

### Compile a debug APK and install the APK to your connected device
```bash
./gradlew installDebug 
```

### Screenshots 
#### Main Screen
![Main Screen](https://w3.calantas.org/wp-content/uploads/2022/06/Screenshot_20220607-104530_myGeo.png)

#### Edit Template Screen
![Edit Template Screen](https://w3.calantas.org/wp-content/uploads/2022/06/Screenshot_20220607-104555_myGeo.png)

#### Setting Screen
![Setting Screen](https://w3.calantas.org/wp-content/uploads/2022/06/Screenshot_20220607-104544_myGeo.png)

### License
GNU General Public License, version 3

![GNU General Public License, version 3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png "GNU General Public License, version 3")

