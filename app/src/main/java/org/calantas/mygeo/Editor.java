/**  
License: GPL-3.0-or-later
Author: veto@myridia.com
Description: Collection of reused methods for this app
*/
package org.calantas.mygeo;
import org.calantas.mygeo.Lib;
import android.os.Bundle;
import android.app.Dialog;
import android.content.Intent;
import android.content.Context; 
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;  
import android.net.Uri;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
public class Editor extends AppCompatActivity {
  Lib lib;
  Dialog dialog;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      lib = new Lib(getApplicationContext());

      setContentView(R.layout.activity_editor);

      Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
      toolbar.setTitle("myGeo - Edit Template");
      setSupportActionBar(toolbar);
      toolbar.setNavigationOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
          logme("...click logo icon");
          Intent intent = new Intent(getApplicationContext(), Main.class);
          intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
          startActivity(intent);
        }
        });



      logme("...get saved draft or default template");
      EditText e = (EditText) findViewById(R.id.editor);
      Context context = getApplicationContext();
      SharedPreferences pref = context.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
      String t = lib.get_template();
      String template = pref.getString("template", t);
      e.setText(template);






    final Button btn_save = findViewById(R.id.btn_save);
    btn_save.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View v)
      {
        logme("...save to preferences");
        EditText e = (EditText) findViewById(R.id.editor);
        String etext = e.getText().toString();

        Context context = getApplicationContext();
        SharedPreferences pref = context.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("template",etext);
        editor.commit();
      }
    });


    final Button btn_clear = findViewById(R.id.btn_clear);
    btn_clear.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View v)
      {
        logme("...clear editor");
        EditText e = (EditText) findViewById(R.id.editor);
        e.setText("");
      }
    });

    final Button btn_reset = findViewById(R.id.btn_reset);
    btn_reset.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View v)
      {
        logme("...reset and load default template");
        String t = lib.get_template();
        EditText e = (EditText) findViewById(R.id.editor);
        e.setText(t);
      }
    });


    final Button btn_latitude = findViewById(R.id.btn_latitude);
    btn_latitude.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View v)
      {
        logme("...insert latitude placeholder");
        EditText e = (EditText) findViewById(R.id.editor);
        lib.insert_into_editText(e, "{0}");
      }
    });

    final Button btn_longitude = findViewById(R.id.btn_longitude);
    btn_longitude.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View v)
      {
        logme("...insert longitude placeholder");
        EditText e = (EditText) findViewById(R.id.editor);
        lib.insert_into_editText(e, "{1}");

      }
    });




   }





  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    getMenuInflater().inflate(R.menu.menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    return lib.drop_menu(item, this, findViewById(R.id.msg));
  }

 private void logme(String message)
  {
    lib.logme(message, findViewById(R.id.logger));    
  }



}
