/**  
License: GPL-3.0-or-later
Author: veto@myridia.com
Description: Setting Acitivy Screen  
*/
package org.calantas.mygeo;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import org.calantas.mygeo.Lib;
import android.os.Bundle;
import android.content.Intent;
import android.content.Context; 
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Button;  
import android.net.Uri;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;

public class Setting extends PreferenceActivity  {
    private AppCompatDelegate mDelegate;
    Lib lib;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

      getDelegate().installViewFactory();
      getDelegate().onCreate(savedInstanceState);
      super.onCreate(savedInstanceState);
      setContentView(R.layout.settings);

      lib = new Lib(getApplicationContext());
      logme("...on create");

      addPreferencesFromResource(R.layout.preferences);

      Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
      toolbar.setTitle("myGeo - Settings");
      toolbar.inflateMenu(R.menu.menu2);
      toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
          logme("... option item select");
          Context context = getApplicationContext();
          return lib.drop_menu(item, context, findViewById(R.id.msg));          
        }
      });
  

      setSupportActionBar(toolbar);
      toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v)
      {
        logme("...click logo icon");
        Intent intent = new Intent(getApplicationContext(), Main.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
      }
      });

    }

    @Override
    protected void onPostCreate(Bundle instance)
    {
      super.onPostCreate(instance);
      getDelegate().onPostCreate(instance);
      logme("...on post create");
    }

    @Override
    public void setContentView(@LayoutRes int layout_id)
    {
      getDelegate().setContentView(layout_id);
      //logme("...set content view");
    }

    @Override
    protected void onPostResume()
    {
      super.onPostResume();
      getDelegate().onPostResume();
    }

    @Override
    protected void onStop()
    {
      super.onStop();
      getDelegate().onStop();
    }

    @Override
    protected void onDestroy()
    {
      super.onDestroy();
      getDelegate().onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
      logme("... option item select");
      return lib.drop_menu(item, this, findViewById(R.id.msg));
    }


    private void setSupportActionBar(@Nullable Toolbar toolbar)
    {
       //getDelegate().setSupportActionBar(toolbar);
    }

    private AppCompatDelegate getDelegate()
    {
      if (mDelegate == null)
      {
        mDelegate = AppCompatDelegate.create(this, null);
      }
      return mDelegate;
    }

 private void logme(String message)
  {
    lib.logme(message, findViewById(R.id.logger_setting));    
  }


}
