
./gradlew assembleRelease
rm ./keystores/mygeo.keystore
keytool -genkey -v -keystore ./keystores/mygeo.keystore -alias Mygeo -keyalg RSA -keysize 2048 -validity 100000
apksigner sign --ks keystores/mygeo.keystore app/build/outputs/apk/release/app-release-unsigned.apk
mv  app/build/outputs/apk/release/app-release-unsigned.apk app/build/outputs/apk/release/app-release-signed.apk
adb install app/build/outputs/apk/release/app-release-signed.apk
